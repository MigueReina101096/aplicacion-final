import { EstaLogeadoGuard } from './autenticacion/esta-logueado.guard';
import { LoginComponent } from './login/login.component';
import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { RutaAplicacionComponent } from './ruta-aplicacion/ruta-aplicacion.component';


const rutas: Route[] = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'home',
        component: RutaAplicacionComponent,
        canActivate:[EstaLogeadoGuard],
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    }
]


@NgModule({
    imports: [RouterModule.forRoot(rutas, {useHash: true})],
    exports: [RouterModule],
    declarations: []
})

export class RutasModule {

}