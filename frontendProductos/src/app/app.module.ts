import { AutenticacionModule } from './autenticacion/autenticacion.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RutaAplicacionComponent } from './ruta-aplicacion/ruta-aplicacion.component';
import { RutasModule } from './app.route.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RutaAplicacionComponent
  ],
  imports: [
    BrowserModule,
    RutasModule,
    AutenticacionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
