import { AutenticacionService } from './../autenticacion/autenticacion.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private readonly _autenticacionService: AutenticacionService) { }

  ngOnInit() {
  }

  login() {
    const usaurioLogeado = this._autenticacionService.login('cristhian.jumbo@epn.edu.ec', 'manticoreLabs1*')
    console.log(usaurioLogeado)
    if(usaurioLogeado){
      this._autenticacionService.estaLogueado = true
    }
  }
}
