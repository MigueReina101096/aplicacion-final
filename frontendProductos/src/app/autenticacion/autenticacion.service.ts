import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Router } from '@angular/router';

@Injectable()
export class AutenticacionService {

    estaLogueado: boolean = false;


    constructor(
        private readonly _httpClient: HttpClient,
        private readonly _router: Router
    ) {

    }

    login(
        correo: string,
        password: string
    ) {
        let usuario;
        const credenciales = { correo, password };
        const respuestaLogin$ = this._httpClient.post(environment.url + '/autenticacion/login', credenciales);
        respuestaLogin$.subscribe(
            (usuarioLogeado) => {
                this.estaLogueado = true;
                console.log(usuarioLogeado); 
                this._router.navigate(['/home']);
            },
            (error) => {
                console.error(error);
                alert('Credenciales invalidas!!');
            }
        )
        return usuario;
    }
}