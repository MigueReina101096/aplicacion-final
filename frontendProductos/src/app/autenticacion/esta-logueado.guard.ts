import { AutenticacionService } from './autenticacion.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class EstaLogeadoGuard implements CanActivate{
    
    constructor(private readonly _autenticacionService: AutenticacionService){

    }
    canActivate(parametroRuta:ActivatedRouteSnapshot){
        return this._autenticacionService.estaLogueado
    }
}