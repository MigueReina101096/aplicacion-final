import { AutenticacionService } from './autenticacion.service';
import { NgModule } from "@angular/core";
import { EstaLogeadoGuard } from './esta-logueado.guard';
import { HttpClientModule } from '@angular/common/http'

@NgModule({
    imports:[HttpClientModule],
    exports:[],
    declarations:[],
    providers:[AutenticacionService,EstaLogeadoGuard],
})
export class AutenticacionModule {}