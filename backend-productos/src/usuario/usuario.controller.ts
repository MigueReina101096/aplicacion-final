import { Controller, Get, Param, Post, Body, Put } from "@nestjs/common";
import { UsuarioService } from "./usuario.service";
import { UsuarioDto } from './dto/usuario.dto'

@Controller('usuario')
export class UsuarioController {
    constructor(
        private readonly _usuarioservice: UsuarioService
    ){}
    @Get()
    obtenerTodos(){
        return this._usuarioservice.buscarTodos()
    }
    @Get('buscar/:id')
    obtenerUno(
        @Param() idUsuario
    ){
        return this._usuarioservice.buscarUno(idUsuario)
    }
    @Post('crear')
    crear(
        @Body() usuarioDto: UsuarioDto
    ){
        return this._usuarioservice.crear(usuarioDto)
    }
    @Post('editar/:id')
    eliminar(
        @Param() idUsuario
    ){
        return this._usuarioservice.eliminar(idUsuario)
    }
    @Put('editar/:id')
    editar(
        @Param() idUsuario,
        @Body() usuarioDto: UsuarioDto
    ){
        return this._usuarioservice.editar(idUsuario,usuarioDto)
    }
}