import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('usuario_entity')
export class UsuarioEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({type: 'varchar', name: 'nombre', default: null})
    nombre: string;
    @Column({type: 'varchar', name: 'correo', unique: true})
    correo: string;
    @Column({type: 'varchar', name:'password'})
    password: string;
}