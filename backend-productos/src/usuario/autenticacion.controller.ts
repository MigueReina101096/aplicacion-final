import { UsuarioService } from './usuario.service';
import { Controller, Post, Body, BadRequestException, Get, createRouteParamDecorator } from "@nestjs/common";
import { UsuarioLoginDto } from "./usuario-login-dto/usuario-login-dto";
import { validate } from "class-validator";
import { generaUsuarioLogin } from './funciones/generar-usuario-login';
var md5 = require('md5');

@Controller('autenticacion')
export class AutenticacionController {
    constructor(private readonly _usuarioService: UsuarioService) { }
    @Post('login')
    async login(
        @Body('correo') correo,
        @Body('password') password,
    ) {
        const usuarioALogearse = generaUsuarioLogin(correo, password)
        const arregloErrores = await validate(usuarioALogearse)
        const existenErrores = arregloErrores.length > 0;
        if (existenErrores) {
            console.log('Errores', arregloErrores)
            throw new BadRequestException('Parametros incorrectos')
        } else {
            console.log(correo)
            const usuarioEncontrado = await this._usuarioService.findOneEmail(usuarioALogearse.correo)
            if (usuarioEncontrado) {
                if (usuarioALogearse.password === usuarioEncontrado.password && usuarioALogearse.correo === usuarioEncontrado.correo) {
                    return usuarioEncontrado
                } else {
                    console.log('Password incorrecto')
                    throw new BadRequestException('Password erroneo')
                }
            } else {
                console.error('Intento fallido: no esxite el usuario ', usuarioALogearse)
                throw new BadRequestException('Error al loguearse')
            }
        }
    }
    @Post('crear')
    async crearUsuario(
        @Body('correo') correo,
        @Body('password') password,
    ) {
        const usuarioALogearse = generaUsuarioLogin(correo, password)
        const arregloErrores = await validate(usuarioALogearse)
        const existenErrores = arregloErrores.length > 0;
        if (existenErrores) {
            console.log('Errores', arregloErrores)
            throw new BadRequestException('Parametros incorrectos')
        } else {
            const usuarioEncontrado = await this._usuarioService.crear(usuarioALogearse)
            return usuarioEncontrado;
        }
    }
    @Get('listar')
    async listarUsuarios() {
        const usuariosLista = await this._usuarioService.buscarTodos()
        return usuariosLista;
    }
}
