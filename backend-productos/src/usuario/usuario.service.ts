import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioEntity } from './usuario.entity';
import { Repository, FindOneOptions } from 'typeorm';
@Injectable()
export class UsuarioService {
    constructor(
        @InjectRepository(UsuarioEntity)
        private readonly _usuarioRepository: Repository<UsuarioEntity>
    ) { }
    async buscarTodos(): Promise<any> {
        return await this._usuarioRepository.find()
    }
    async buscarUno(id: number): Promise<any> {
        return await this._usuarioRepository.findOne(id)
    }
    async crear(usuario): Promise<any> {
        const variable = this._usuarioRepository.create(usuario)
        return await this._usuarioRepository.save(variable)
    }
    async eliminar(id: number): Promise<any> {
        return await this._usuarioRepository.delete(id)
    }
    async editar(id: number, usuario): Promise<any> {
        return await this._usuarioRepository.update(id, usuario)
    }
    async findOneEmail(correo: string): Promise<any> {
        const opciones: FindOneOptions = {
            where: {
                correo
            }
        }
        const usuarioEncontrado = this._usuarioRepository.findOne(undefined, opciones)
        return usuarioEncontrado
    }
    async findOnePassword(password: string): Promise<any> {
        const opciones: FindOneOptions = {
            where: {
                password
            }
        }
        const usuarioEncontrado = this._usuarioRepository.findOne(undefined, opciones)
        return usuarioEncontrado
    }
    
}