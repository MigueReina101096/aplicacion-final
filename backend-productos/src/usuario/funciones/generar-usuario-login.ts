import { UsuarioLoginDto } from './../usuario-login-dto/usuario-login-dto';

export function generaUsuarioLogin(correo: string, password: string): UsuarioLoginDto {
    const usuarioLogin = new UsuarioLoginDto();
    usuarioLogin.correo = correo;
    usuarioLogin.password = password;
    return usuarioLogin;
} 