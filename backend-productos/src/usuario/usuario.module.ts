import { AutenticacionController } from './autenticacion.controller';
import { Module } from "@nestjs/common";
import { UsuarioController } from "./usuario.controller";
import { UsuarioResolver } from "./usuario.resolver";
import { UsuarioService } from "./usuario.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UsuarioEntity } from "./usuario.entity";

@Module({
    imports:[
        TypeOrmModule.forFeature([
            UsuarioEntity
        ])
    ],
    controllers:[UsuarioController,AutenticacionController],
    providers:[UsuarioResolver, UsuarioService],
})
export class UsuarioModule {}